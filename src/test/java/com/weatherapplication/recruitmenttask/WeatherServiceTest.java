package com.weatherapplication.recruitmenttask;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
@ExtendWith(MockitoExtension.class)
public class WeatherServiceTest {

    @Mock
    private InvocationDataRepository invocationDataRepository;
    private WeatherService weatherService;
    @BeforeEach
    void setUp() {
        weatherService = new WeatherService(invocationDataRepository);
    }
    @Test
    public void getDataFromOpenMeteo_test_responce_data () {
        //Assign
        WeatherData[] expectedWeatherData = new WeatherData[7];
        expectedWeatherData[0] = new WeatherData("2023-05-25T05:56","2023-05-25T19:12",0,"2023-05-25");
        expectedWeatherData[1] = new WeatherData("2023-05-26T05:56","2023-05-26T19:13",0,"2023-05-26");
        expectedWeatherData[2] = new WeatherData("2023-05-27T05:56","2023-05-27T19:13",0,"2023-05-27");
        expectedWeatherData[3] = new WeatherData("2023-05-28T05:56","2023-05-28T19:14",0,"2023-05-28");
        expectedWeatherData[4] = new WeatherData("2023-05-29T05:56","2023-05-29T19:14",0,"2023-05-29");
        expectedWeatherData[5] = new WeatherData("2023-05-30T05:56","2023-05-30T19:14",0,"2023-05-30");
        expectedWeatherData[6] = new WeatherData("2023-05-31T05:55","2023-05-31T19:15",0,"2023-05-31");

//        Mockito.when(invocationDataRepository.save(Mockito.any(InvocationData.class))).thenAnswer(i -> i.getArguments()[0]);
        //Act
        WeatherData[] weatherData = weatherService.getWeatherData(20.5,20.5);
        //Assert
        for (int i = 0; i < 7; i++) {
            Assertions.assertEquals(expectedWeatherData[i].getSunrise(), weatherData[i].getSunrise());
            Assertions.assertEquals(expectedWeatherData[i].getSunset(), weatherData[i].getSunset());
            Assertions.assertEquals(expectedWeatherData[i].getDate(), weatherData[i].getDate());
            Assertions.assertEquals(expectedWeatherData[i].getAverageRainFall(), weatherData[i].getAverageRainFall());
        }
    }

}
