package com.weatherapplication.recruitmenttask;

import lombok.AllArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

@Service
@AllArgsConstructor
public class WeatherService {
    InvocationDataRepository invocationDataRepository;
    public WeatherData[] getWeatherData(double longitude, double latitude) {
        WeatherData[] data = getDataFromOpenMeteo(longitude, latitude);
        return data;
    }

    private WeatherData[] getDataFromOpenMeteo(double longitude, double latitude) {
        String url = "https://api.open-meteo.com/v1/forecast?" +
                "longitude={longitude}&" +
                "latitude={latitude}&" +
                "daily=sunrise,sunset,rain_sum&" +
                "start_date={startDate}&" +
                "end_date={endDate}&" +
                "timezone={timezone}";
        RestTemplate restTemplate = new RestTemplate();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String responceDataString = restTemplate.getForObject(url,
                String.class,
                longitude,
                latitude,
                LocalDateTime.now().minusDays(7).format(dateTimeFormatter),
                LocalDateTime.now().minusDays(1).format(dateTimeFormatter),
                TimeZone.getDefault().getID());

        JSONObject responceData = new JSONObject(responceDataString);
        JSONObject daily = (JSONObject) responceData.get("daily");
        JSONArray sunrises = daily.getJSONArray("sunrise");
        JSONArray sunsets = daily.getJSONArray("sunset");
        JSONArray rainSums = daily.getJSONArray("rain_sum");
        JSONArray dates = daily.getJSONArray("time");

        WeatherData[] weatherData = new WeatherData[7];
        for (int i = 0; i < 7; i++) {
            WeatherData weatherDataOfADay = new WeatherData(
                sunrises.getString(i),
                sunsets.getString(i),
    rainSums.getLong(i)/24,
                dates.getString(i)
            );
            weatherData[i] = weatherDataOfADay;
        }
        invocationDataRepository.save(new InvocationData(LocalDateTime.now(), longitude, latitude));
        return weatherData;
    }
}
