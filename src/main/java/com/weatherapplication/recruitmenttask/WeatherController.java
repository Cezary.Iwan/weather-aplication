package com.weatherapplication.recruitmenttask;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WeatherController {

    private final WeatherService weatherService;

    public WeatherController(WeatherService weatherService) {
        this.weatherService = weatherService;
    }

    @GetMapping("/getData")
    public WeatherData[] getWeatherData (@RequestParam("longitude") double longitude, @RequestParam("latitude") double latitude) throws JsonProcessingException {
        return weatherService.getWeatherData(longitude, latitude);
    }
}
