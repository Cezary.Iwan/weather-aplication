package com.weatherapplication.recruitmenttask;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Date;
@Getter
@Setter
@Entity(name = "invocation_data")
public class InvocationData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "invocation_date")
    private LocalDateTime invocationDate;
    @Column(name = "longitude")
    private double longitude;
    @Column(name = "latitude")
    private double latitude;

    public InvocationData(LocalDateTime invocationDate, double longitude, double latitude) {
        this.invocationDate = invocationDate;
        this.longitude = longitude;
        this.latitude = latitude;
    }

}
