package com.weatherapplication.recruitmenttask;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WeatherData {
    private final String sunrise;
    private final String sunset;
    private final long averageRainFall;
    private final String date;

    public WeatherData(String sunrise, String sunset, long averageRainFall, String date) {
        this.sunrise = sunrise;
        this.sunset = sunset;
        this.averageRainFall = averageRainFall;
        this.date = date;
    }
}
