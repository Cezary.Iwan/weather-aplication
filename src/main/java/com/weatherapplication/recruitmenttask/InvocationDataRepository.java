package com.weatherapplication.recruitmenttask;

import org.springframework.data.jpa.repository.JpaRepository;

public interface InvocationDataRepository extends JpaRepository<InvocationData, Long> {

}
